package hello;

import interfaces.Hello;
import cloud.orbit.actors.Actor;
import cloud.orbit.actors.Stage;
import org.apache.log4j.BasicConfigurator;

public class Main {
    public static void main(String[] args) throws Exception
    {
        BasicConfigurator.configure();

        Stage stage = new Stage.Builder().clusterName("orbit-helloworld-cluster").build();
        stage.start().join();
        stage.bind();

        // Send a message to the actor
        final String response = Actor.getReference(Hello.class, "0").sayHello("Welcome to orbit").join();
        System.out.println(response);

        // Shut down the stage
        stage.stop().join();
    }
}
