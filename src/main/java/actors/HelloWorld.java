package actors;
import cloud.orbit.actors.runtime.AbstractActor;
import cloud.orbit.concurrent.Task;
import interfaces.Hello;

public class HelloWorld extends AbstractActor implements Hello {
    public Task<String> sayHello(String greeting) {
        System.out.println("Here: " + greeting);
        return Task.fromValue(
                "You said '" + greeting + "', I say: hello from " + System.identityHashCode(this) + " !"
        );
    }
}
